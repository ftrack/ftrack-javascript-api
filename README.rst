###############################
ftrack javascript api
###############################

############
MOVED TO GITHUB: https://github.com/ftrackhq/javascript-api
############

This is a deprecated repository for ftrack's javascript API, which nowadays lives on Github. 

Please find it here: https://github.com/ftrackhq/javascript-api